with open('04/input.txt') as input_file:
    sections = input_file.readlines()


def list_elf_sections(section: str) -> list[set]:
    section = section.strip()
    elf_sections = section.split(',')
    elf_section = [(section.split('-')) for section in elf_sections]
    return [set(range(int(section[0]), int(section[1])+1)) for section in elf_section]


def is_subset(elf_sections: list[set]) -> bool:
    return True if elf_sections[0] & elf_sections[1] == elf_sections[0] or elf_sections[1] & elf_sections[0] == elf_sections[1] else False


def are_overlapped(elf_sections: list[set]) -> bool:
    return True if elf_sections[0] & elf_sections[1] else False


same_sections = 0
have_overlap = 0
for section in sections:
    section_set = list_elf_sections(section)
    if is_subset(section_set):
        same_sections += 1
    if are_overlapped(section_set):
        have_overlap += 1
print(same_sections)
print(have_overlap)