def score_for_shape(shapes: list):
    victory_modifier = ord(shapes[1]) - 89
    opponent_shape_score = ord(shapes[0]) - 64
    shape_modifier = opponent_shape_score + victory_modifier
    if shape_modifier < 1:
        score = 3
    elif shape_modifier > 3:
        score = 1
    else:
        score = shape_modifier
    return score


def score_for_result(shape):
    scores = {
        'X': 0,
        'Y': 3,
        'Z': 6,
    }
    return scores[shape]


def rps_score(all_shapes):
    score = 0
    for shapes in all_shapes:
        score += score_for_shape(shapes)
        score += score_for_result(shapes[1])
    return score


with open('02/input.txt') as input_file:
    all_shapes = [line.rstrip().split(' ') for line in input_file]

print(rps_score(all_shapes))
