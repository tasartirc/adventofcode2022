with open('01/input.txt') as input_file:
    inventory = input_file.read().strip()


def elves(input_calories):
    elves_calories = input_calories.split('\n\n')
    return elves_calories


def sum_calories(elf):
    calories = [int(calory.strip()) for calory in elf.split('\n')]
    elf_calories = 0
    for calory in calories:
        elf_calories += calory
    return elf_calories


def select_richest_elves(calories, count):
    sum_elf_calories = [sum_calories(elf) for elf in elves(calories)]
    sum_elf_calories.sort()
    sum_richest_elves = sum_elf_calories[-count:]
    richest_elves = sum(sum_richest_elves)
    return richest_elves


print(select_richest_elves(inventory, 1))
print(select_richest_elves(inventory, 3))