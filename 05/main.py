with open('input.txt') as input_file:
    lines = input_file.readlines()
lines = [line.rstrip('\n') for line in lines]


def parse_lines(lines: list) -> list[int, list[int]]:
    # get number of line with column numbers definition
    for line_number, line in enumerate(lines):
        if line[1] == '1':
            line_with_columns = line_number
            break

    # get number of columns where items are written
    column_positions = []
    for character_number, character in enumerate(lines[line_with_columns]):
        if character != ' ':
            column_positions.extend([character_number])
    return [line_with_columns, column_positions]


def parse_columns(lines: list, column_positions: list[int]) -> list[list[str]]:
    columns = [[]]*len(column_positions)
    for line in lines[::-1]:
        for column_number, position in enumerate(column_positions):
            value = line[position].strip()
            if value != '':
                columns[column_number] = columns[column_number] + [value]
    return columns


def parse_instruction(instruction: str) -> list[int, int, int]:
    instruction = instruction.split(' ')
    move_quantity = int(instruction[1])
    move_from = int(instruction[3])
    move_to = int(instruction[5])
    return [move_quantity, move_from, move_to]


def do_instruction(parsed_instruction: list[int, int, int], columns: list[list[str]], pick_multiple = False):
    move_quantity, move_from, move_to = parsed_instruction

    if not pick_multiple:
        done_quantities = 0
        while done_quantities < move_quantity:
            moved_item = columns[move_from-1].pop(-1)
            columns[move_to-1].append(moved_item)
            done_quantities += 1

    if pick_multiple:
        moved_items = columns[move_from-1][-move_quantity:]
        del columns[move_from-1][-move_quantity:]
        columns[move_to-1].extend(moved_items)


def show_result(columns: list[list[str]]):
    result = ''
    for column in columns:
        result += str(column[-1])
    return result


line_with_columns, column_positions = parse_lines(lines)
columns = parse_columns(lines[:line_with_columns], column_positions)

for line_number, line in enumerate(lines):
    if line_number > line_with_columns+1:
        parsed_instruction = parse_instruction(line)
        do_instruction(parsed_instruction, columns, True)


result = show_result(columns)

print(result)