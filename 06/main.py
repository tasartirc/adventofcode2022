with open('input.txt') as input_file:
    code = input_file.read().strip()


def marker_location(code: str, sequence_length=4) -> int:
    for i in range(0, len(code)-(sequence_length+1)):
        sequence = code[i:i+sequence_length]
        duplicate_characters = False
        for character in sequence:
            if sequence.count(character) > 1:
                duplicate_characters = True
                break
        if not duplicate_characters:
            return i+sequence_length


print(marker_location(code, 4))
print(marker_location(code, 14))