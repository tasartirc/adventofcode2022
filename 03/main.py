with open('03/input.txt') as input_file:
    bags = input_file.readlines()


def common_item(bag: str) -> str:
    bag = bag.strip()
    length = len(bag)
    compartments = [bag[:length//2], bag[length//2:]]
    common_item_letter_set = set(compartments[0]) & set(compartments[1])
    common_item_letter = ''.join(common_item_letter_set)[0]
    return common_item_letter


def priority_value(item: str) -> int:
    value = ord(item)
    if value > 96:
        value -= 96
    else:
        value -= 38
    return value


def common_item_in_group(bags: list) -> str:
    common_item_letter_set = set(bags[0].strip()) & set(bags[1].strip()) & set(bags[2].strip())
    common_item_letter = ''.join(common_item_letter_set)[0]
    return common_item_letter


value = 0
for items in bags:
    value += priority_value(common_item(items))
print(value)

group_value = 0
for bag in range(0, len(bags), 3):
    group_of_bags = [bags[bag], bags[bag+1], bags[bag+2]]
    group_value += priority_value(common_item_in_group(group_of_bags))
print(group_value)
